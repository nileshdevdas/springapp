package com.db.microservices;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AirportController {

	@Autowired
	private IAirportService service;

	@GetMapping(value = "findByCode/{code}")
	public List<Airport> findAirportByCode(@PathVariable("code") String code) {
		return service.findAirportByCode(code);
	}

	@GetMapping(value = "findByName/{name}")
	public List<Airport> findAirportByName(@PathVariable("name") String name) {
		return service.findAirportByName(name);
	}

	@GetMapping(value = "findByLatitude/{latitude}")
	public List<Airport> findAirportByLatitude(@PathVariable("latitude") String name) {
		return service.findAirportByLatitude(name);
	}

}
