package com.db.microservices;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

@Service
public class AirportService implements IAirportService {

	// @Value("${file.location}")
	// private String fileLocation;

	private static List<Airport> airports = null;

	public AirportService() {
		try {
			airports = Files.readAllLines(Paths.get("d:\\airportsdata\\airports.csv")).stream().skip(1)
					.map(AirportService::stringToAirport).collect(Collectors.toList());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Airport> findAirportByCode(String code) {
		return airports.stream().filter((each) -> each.getCode().contains(code)).collect(Collectors.toList());
	}

	public List<Airport> findAirportByName(String name) {
		return airports.stream().filter((each) -> each.getName().contains(name)).collect(Collectors.toList());
	}

	public List<Airport> findAirportByLatitude(String latitude) {
		return airports.stream().filter((each) -> each.getLatitude().contains(latitude)).collect(Collectors.toList());
	}

	public static Airport stringToAirport(String row) {
		String trimmedData = row.replaceAll("\"", "");
		String[] cols = trimmedData.split(",");
		Airport airport = new Airport();
		airport.setCode(cols[0]);
		airport.setName(cols[3]);
		airport.setLatitude(cols[4]);
		airport.setLongitude(cols[5]);
		return airport;
	}
}
